def get_slug(text, sep='-'):
    text = text.lower()
    rus = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш',
           'щ', 'ы', 'ь', 'э', 'ю', 'я',
           'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'v', 'z',
           '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
           ' ', '-', '_', '+', '(', ')', '=', ]
    lat = ['a', 'b', 'v', 'g', 'd', 'e', 'yo', 'j', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch',
           'sh', 'csh', 'yi', '', 'e', 'yu', 'ya',
           'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'v', 'z',
           '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
           sep, sep, sep, sep, sep, sep, sep, ]
    
    return_text = ''
    for let in text:
        if not let in rus:
            continue
        return_text += lat[rus.index(let)]
    
    return return_text