from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User

from tst.utils import get_slug

import time


# эту функцию можно просто запихнуть в save, но т.к. у нас профиль привязывается через декораторы
# (см. ниже класса профиля)
# то пришлось эту функцию вынести отдельно, а в других местах (например в категориях) используется переопределение save()
def get_profile_image_folder(instance, filename):
    # filename = instance.user.username + '.' + filename.split('.')[1]
    filename = get_slug(instance.user.username) + str(time.time())[-4:] + '.' + filename.split('.')[-1]
    return 'img-profile/' + filename


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # image = models.ImageField(upload_to=get_profile_image_folder, default='img/blank-profile.png', verbose_name='')
    # verbose_name пустой стоит потому что после него идут дефолтные значения и перед ним тоже
    
    # image = ThumbnailerImageField(upload_to=get_profile_image_folder, default='img/blank-profile.png', verbose_name='',
    #                               resize_source=dict(quality=95, size=(150, 150)))
    city = models.CharField(max_length=100, blank=True)
    address = models.CharField(max_length=150, blank=True)
    phone = models.CharField(max_length=30, blank=True)
    company = models.CharField(max_length=100, blank=True)
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=15)
    
    class Meta:
        verbose_name = "Профиль! пользователя"
        verbose_name_plural = "Профили! пользователей"
    
    def __str__(self):
        return "{0} ({1})".format(self.user.username, self.user.get_full_name())
    
    def profile_fill_check(self):
        check = True
        if len(self.user.first_name) < 2 or len(self.user.last_name) < 2 or len(self.user.email) < 7 or "@" not in self.user.email:
            check = False
        if len(self.city) < 3 or len(self.address) < 5 or len(self.phone) < 10 or len(self.company) < 3:
            check = False
        
        return check
    


# post_save.connect(my_function, MyModel) # для сохранки использование другого метода сигналов

# Мы «зацепили» create_user_profile() и save_user_profile() к событию сохранения модели User. Такой сигнал называется post_save.
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, created, **kwargs):
    instance.profile.save()
