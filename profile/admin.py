from django.contrib import admin
from django.utils.safestring import mark_safe

from profile.models import Profile

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'get_thumb_img', 'city', 'phone', 'company', 'discount')
    readonly_fields = ['get_thumb_img100', 'get_thumb_img']
    fields = ['user', 'get_thumb_img100', 'image', 'city', 'address', 'phone', 'company', 'discount',]

    def get_thumb_img100(self, obj):
        return self.get_thumb_img(obj, 100)

    get_thumb_img100.short_description = 'Картинка'
    get_thumb_img100.allow_tags = True

    def get_thumb_img(self, obj, width=40):
        if obj.image:
            return mark_safe('<a href="{0}" target="_blank"><img src="{0}" width="{1}"/></a>'.format(obj.image.url, width))
        else:
            return '(Нет изображения)'

    get_thumb_img.short_description = 'Картинка'
    get_thumb_img.allow_tags = True
    
admin.site.register(Profile, ProfileAdmin)
