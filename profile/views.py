# from django.http import HttpResponseRedirect, JsonResponse, Http404
# from django.shortcuts import render, reverse, get_object_or_404, redirect
# from django.db.models import Q, F, Count
#
# from stock.forms import OrderForm
#
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import Group
# from django.contrib.auth import login, authenticate
#
# from client.forms import UserForm, ProfileForm, LoginForm, RegistrationForm, ProfilePhoneForm
# from catalogue.models import Category, Product
# from stock.models import OrderItem, Order
# from sending_email.models import    send_notification_about_new_registration, \
#                                     send_notification_about_client_has_sent_new_order, \
#                                     send_notification_about_need_to_pay_order
#
#
#
#
# # def test(request):
# #     num_order = 5
# #     order = Order.objects.get(id=num_order)
# #     order.check_stock()
# #
# #     return render(request, 'client/index.html')
#
#
# def registration(request):
#     if request.user.is_authenticated:
#         return HttpResponseRedirect(reverse('account'))
#
#     if request.method == 'POST':
#         form = RegistrationForm(request.POST or None)
#         form_phone = ProfilePhoneForm(request.POST or None)
#         if form.is_valid() and form_phone.is_valid():
#             user = form.save()
#             user.profile.phone = form_phone.cleaned_data['phone']
#             user.set_password(form.cleaned_data['password'])
#             user.is_active = False  # при регистрации пользователь неактивен, активировать его может только администратор
#             user.save()
#
#             # после собственно регистрации (сохранения нового) пользователя его можно добавить к группам
#             user.groups.add(Group.objects.get(name='clients'))
#
#             # после регистрации, отправляем письмо администратору.
#             send_notification_about_new_registration(user)
#
#             return HttpResponseRedirect(reverse('client:thank_you_register'))
#     else:
#         form = RegistrationForm()
#         form_phone = ProfilePhoneForm()
#
#     context = {
#         'title': 'Регистрация',
#         'form': form,
#         'form_phone': form_phone,
#     }
#     return render(request, 'client/registration.html', context)
#
#
# def login_view(request):
#     # проверяем, а возможно нужно на страницу розницы
#     if request.COOKIES.get('cabinet') == 'retail':
#         return redirect(reverse('retail:login'))
#
#     if request.user.is_authenticated:
#         return HttpResponseRedirect(reverse('account'))
#
#     form = LoginForm(request.POST or None)
#
#     if form.is_valid():
#         username = form.cleaned_data['username']
#         password = form.cleaned_data['password']
#
#         login_user = authenticate(request, username=username, password=password)
#
#         # если зареган, то логиниться
#         if login_user:
#             login(request, login_user)
#             return HttpResponseRedirect(reverse('account'))
#
#     context = {
#         'title': 'Вход в систему',
#         'form': form,
#     }
#     return render(request, 'login.html', context)
#
# @login_required
# def client_products(request):
#     products = Product.objects.ava().order_by('-category__priority', '-priority', 'title')
#     context = {
#         'title': 'Каталог всех изделий',
#         'products': products,
#     }
#     return render(request, 'client/products.html', context=context)
#
# @login_required
# def client_categories(request):
#     categories = Category.objects.all().order_by('-priority', 'name')
#     context = {
#         'title': 'Список категорий изделий',
#         'categories': categories,
#     }
#     return render(request, 'client/categories.html', context=context)
#
# @login_required
# def client_category_one(request, slug):
#     category = Category.objects.get(slug=slug)
#     products = Product.objects.filter(category=category).order_by('-priority', 'title')
#     context = {
#         'title': 'Изделия категории ' + category.name,
#         'category': category,
#         'products': products,
#     }
#
#     return render(request, 'client/category_one.html', context=context)
#
# @login_required
# def client_product_one(request, slug):
#     product = Product.objects.get(slug=slug)
#     product.price_furniture = round(product.price * ((100 - request.user.profile.discount) / 100), 2)
#
#     context = {
#         'title': product.title,
#         'product': product,
#     }
#     return render(request, 'client/product_one.html', context=context)
#
# #   Q нужно использовать для поиска сразу по нескольким полям
# #   т.к. сейчас поиск происходит лишь по одному полю, то выборка через Q отключена
# # def get_search_products(query, order=False, values=False):
# #     if ' ' in query and query != ' ':
# #         queries = query.split(' ')
# #         len_queries = len(queries)
# #         products = Product.objects.filter(Q(title__icontains=queries[0]))
# #         for key in range(1, len_queries):
# #             products = products.filter(Q(title__icontains=queries[key]))
# #     else:
# #         products = Product.objects.filter(Q(title__icontains=query))
# #
# #     if values:
# #         if order:
# #             products = products.exclude(orderitem__order=order)
# #         return products.values()[0:5]
# #     else:
# #         return products
#
#
# def get_search_products_non_q(query, order=False, values=False):
#     if ' ' in query and query != ' ':
#         queries = query.split()
#         len_queries = len(queries)
#         products = Product.objects.filter(title__icontains=queries[0])
#         for key in range(1, len_queries):
#             products = products.filter(title__icontains=queries[key])
#     else:
#         products = Product.objects.filter(title__icontains=query)
#
#     if values:
#         if order:
#             products = products.exclude(orderitem__order=order)
#         return products.values()[0:10]
#     else:
#         return products
#
# @login_required
# def client_search(request):
#     query = request.GET.get('q')
#     order = request.GET.get('o')
#
#     if order == None:
#         order = False
#
#     if query != None:
#         products = get_search_products_non_q(query, order)
#         # products = get_search_products(query, order)
#         context = {
#             'title': 'Результаты поиска по запросу: ' + query,
#             'products': products,
#         }
#     else:
#         context = {
#             'title': 'Введите строку поиска'
#         }
#     return render(request, 'client/search.html', context=context)
#
#
# # оставляю функцию на возможное использование. Пока решил заменить её с выводом списка
# @login_required
# def client_search_json(request):
#     query = request.GET.get('q')
#     order = request.GET.get('o')
#
#     if order == None:
#         order = False
#
#     if query != None:
#         products = get_search_products_non_q(query, order, True)
#     else:
#         products = None
#
#     products = list(products)
#
#     return JsonResponse({'result': products, })
#
#
# @login_required
# def client_search_for_js(request):
#     query = request.GET.get('q')
#     order = request.GET.get('o')
#
#     if order == None:
#         order = False
#
#     if query != None:
#         products = get_search_products_non_q(query, order, True)
#         # products = get_search_products(query, order, True)
#     else:
#         products = None
#
#     context = {
#         'products': products,
#     }
#
#     return render(request, 'client/search_for_js.html', context=context)
#
#
# @login_required
# def client_profile_update(request):
#     if request.method == 'POST':
#         user_form = UserForm(request.POST or None, instance=request.user)
#         profile_form = ProfileForm(request.POST or None, request.FILES or None, instance=request.user.profile)
#         if user_form.is_valid() and profile_form.is_valid():
#             user_form.save()
#             profile_form.save()
#             return HttpResponseRedirect(reverse('client:profile'))
#     else:
#         user_form = UserForm(instance=request.user)
#         profile_form = ProfileForm(instance=request.user.profile)
#
#     from easy_thumbnails.files import get_thumbnailer
#
#     context = {
#         'user_form': user_form,
#         'profile_form': profile_form,
#         'image_url': get_thumbnailer(request.user.profile.image)['prof_thumb'].url,
#         'form_url': reverse('client:profile_update'),
#         'title': 'Изменение профиля',
#     }
#     return render(request, 'client/profile_update.html', context=context)
#
#
# @login_required
# def client_profile(request):
#     user = request.user
#     context = {}
#     if (not user.profile.profile_fill_check()):
#         context = {
#             'bad_profile': True,
#         }
#
#     context.update({
#         'user': user,
#     })
#     return render(request, 'client/profile.html', context=context)
#
#
# @login_required
# def orders(request):
#     list_orders = Order.objects.filter(user=request.user).exclude(status='cancel').order_by('-id')
#
#     context = {
#         'title': 'Список всех заказов',
#         'staff': False,
#         'orders': list_orders,
#     }
#     return render(request, 'client/orders.html', context=context)
#
#
# @login_required
# def orders_sent(request):
#     list_orders = Order.objects.filter(user=request.user, is_sent=True).exclude(status='cancel').order_by('-id')
#
#     context = {
#         'title': 'Список подтверждённых заказов',
#         'staff': False,
#         'orders': list_orders,
#     }
#     return render(request, 'client/orders.html', context=context)
#
#
# @login_required
# def orders_not_sent(request):
#     list_orders = Order.objects.filter(user=request.user, is_sent=False).exclude(status='cancel').order_by('-id')
#
#     context = {
#         'title': 'Список не отправленных заказов',
#         'staff': False,
#         'orders': list_orders,
#     }
#     return render(request, 'client/orders.html', context=context)
#
#
# @login_required
# def orders_pay(request):
#     list_orders = Order.objects.filter(user=request.user, is_sent=True).exclude(date_payment=None).exclude(status='cancel').order_by('-id')
#
#     context = {
#         'title': 'Список оплаченных заказов',
#         'staff': False,
#         'orders': list_orders,
#     }
#     return render(request, 'client/orders.html', context=context)
#
#
# @login_required
# def order_vendor(request, id_order):
#     try:
#         # если в сессии есть новый заказ и он его не заполнил, то перекинуть на правку этого заказа
#         if request.session['order_id'] and id_order == request.session['order_id']:
#             return HttpResponseRedirect(reverse('client:order_new'))
#     except:
#         order = get_object_or_404(Order, user=request.user, id=id_order)
#         if order.total_cost < 1:
#             return HttpResponseRedirect(reverse('client:order_edit', args=[order.id]))
#
#     # работа с формой
#     if request.method == 'POST':
#         order = get_object_or_404(Order, user=request.user, id=id_order, is_sent=False)
#         form = request.POST
#
#         order.sent_by_client(form)
#         if (order.check_stock()):
#             order.write_off()
#             # отправляем письмо о необходимости оплаты
#             send_notification_about_need_to_pay_order(order)
#
#         # после подтверждаюей отправки, отправляем письмо администратору.
#         send_notification_about_client_has_sent_new_order(order)
#
#         return HttpResponseRedirect(reverse('client:order_vendor', args=[order.id]))
#
#     try:
#         order = Order.objects.get(user=request.user, id=id_order)
#         order_items = OrderItem.objects.filter(order=order).order_by('-product__category__priority', '-product__priority', 'product__title')
#
#         context = {
#             'title': 'Заказ № ' + str(id_order),
#             'order': order,
#             'order_items': order_items,
#         }
#
#         if order.is_sent:
#             return render(request, 'client/order_is_sent_vendor.html', context=context)
#         else:
#             return render(request, 'client/order_vendor.html', context=context)
#     except:
#         raise Http404("Нет такой страницы, или вам к ней ограничен доступ.")
#
#
# @login_required
# def order_new(request):
#     if (not request.user.profile.profile_fill_check()):
#         return HttpResponseRedirect(reverse('client:profile'))
#
#     # создаём учётную запись заказа, если её ещё нет
#     try:
#         order_id = request.session['order_id']
#         order = Order.objects.get(user=request.user, id=order_id)
#         # order = order[0]
#     except:
#         order = Order()
#         order.user = request.user
#         order.save()
#         request.session['order_id'] = order.id
#
#     # работа с формой
#     if request.method == 'POST':
#         order_form = OrderForm(request.POST or None, instance=order)
#         if order_form.is_valid():
#             order_form.save()
#             del request.session['order_id']
#             return HttpResponseRedirect(reverse('client:order_edit', args=[order.id]))
#     else:
#         order_form = OrderForm(instance=order)
#
#     context = {
#         'title': 'Создание нового заказа. Шаг1 - заполнение информации.',
#         'order': order,
#         'order_form': order_form,
#     }
#     return render(request, 'client/order_create_new.html', context)
#
#
# @login_required
# def order_edit(request, id_order):
#     try:
#         if request.session['order_id'] and id_order == request.session['order_id']:
#             return HttpResponseRedirect(reverse('client:order_new'))
#     except:
#         pass
#
#     order = get_object_or_404(Order, user=request.user, id=id_order)
#     order_items = OrderItem.objects.filter(order=order).order_by('-product__category__priority', '-product__priority', 'product__title')
#
#     if order.is_sent:
#         template = 'client/order_is_sent.html'
#     else:
#         template = 'client/order_edit.html'
#
#     context = {
#         'title': 'Карточка заказа № ' + str(order.id),
#         'order': order,
#         'order_items': order_items,
#     }
#     return render(request, template, context)
#
#
# @login_required
# def order_edit_info(request, id_order):
#     order = get_object_or_404(Order, user=request.user, id=id_order)
#
#     # работа с формой
#     if request.method == 'POST':
#         order_form = OrderForm(request.POST or None, instance=order)
#         if order_form.is_valid():
#             order_form.save()
#             return HttpResponseRedirect(reverse('client:order_edit', args=[order.id]))
#     else:
#         order_form = OrderForm(instance=order)
#
#     context = {
#         'title': 'Редактирование карточки заказа',
#         'order': order,
#         'order_form': order_form,
#     }
#     return render(request, 'client/order_create_new.html', context)
#
#
# @login_required
# def add_to_order(request):
#     id_order = request.GET.get('id_order')
#     order = get_object_or_404(Order, user=request.user, id=id_order)
#
#     id_product = request.GET.get('id_product')
#     product = Product.objects.get(id=id_product)
#
#     item, this_new_item = OrderItem.objects.get_or_create(product=product,
#                                                           order=order,
#                                                           defaults={'price': product.price})
#
#     if not this_new_item:
#         item.qty += 1
#         item.save()
#
#     total_cost = order.update_total_cost()
#     return JsonResponse({'total_cost': total_cost, })
#
#
# @login_required
# def change_item_qty(request):
#     id_order = request.GET.get('id_order')
#     order = get_object_or_404(Order, user=request.user, id=id_order)
#
#     id_product = request.GET.get('id_product')
#
#     qty = int(request.GET.get('qty'))
#
#     item = get_object_or_404(OrderItem, order=id_order, product=id_product)
#     item.qty = qty
#     item.save()
#
#     total_cost = order.update_total_cost()
#
#     return JsonResponse({'total_cost': total_cost})
#
#
# @login_required
# def remove_from_cart(request):
#     id_order = request.GET.get('id_order')
#     order = get_object_or_404(Order, user=request.user, id=id_order)
#
#     id_product = request.GET.get('id_product')
#
#     item = get_object_or_404(OrderItem, order=id_order, product=id_product)
#     item.delete()
#
#     total_cost = order.update_total_cost()
#     return JsonResponse({'total_cost': total_cost, })
