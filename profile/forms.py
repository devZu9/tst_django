from django.http import Http404
from profile.models import Profile
from django import forms
from django.contrib.auth.models import User



# from django.utils import timezone

class RegistrationForm(forms.ModelForm):
    password_check = forms.CharField(widget=forms.PasswordInput, label='Повтор пароля')
    
    class Meta:
        model = User
        # fields = ['username', 'password', 'password_check', 'first_name', 'last_name', 'email']
        fields = ['username', 'password', 'password_check', 'email']
        labels = {
            'username': 'Логин',
            # 'first_name': 'Ваше имя',
            # 'last_name': 'Ваша фамилия',
            'email': 'Email',
            'password': 'Пароль',
        }
        help_texts = {
            'username': 'придумайте себе имя пользователя (латинские и цифры)',
            # 'first_name': '(можно и отчество)',
            'password': 'придумайте пароль не совсем простой',
            'email': 'указывайте реальный адрес, т.к. на него придёт подтверждение'
        }
        widgets = {
            'password': forms.PasswordInput(),
        }
    
    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        password_check = self.cleaned_data['password_check']
        email = self.cleaned_data['email']
        
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError(
                {
                    'username': 'Пожалуйста выберите другое имя пользователя, т.к. пользователь с таким логином уже зарегистрирован в системе!'},
                code='user exists')
        
        if password != password_check:
            raise forms.ValidationError({'password': '',
                                         'password_check': 'Вы ошиблись при вводе паролей, они не совпадают, введите повторно!'},
                                        code='passwords do not match', )
        
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(
                {
                    'email': 'Пользователь с таким email уже зарегистрирован! Возможно вам стоит просто войти или воспользоваться восстановлением пароля.'},
                code='email exists')


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']
        labels = {
            'username': 'Логин',
            'password': 'Пароль',
        }
        help_texts = {
            'username': '',
        }
        
        widgets = {
            'password': forms.PasswordInput(),
        }
    
    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError(
                {'username': '',
                 'password': 'Пользователь с такой парой Логина и Пароля в системе не зарегистрирован!'},
                code='not register')
        
        user = User.objects.get(username=username)
        if user and not user.check_password(password):
            raise forms.ValidationError({
                'username': '',
                'password': 'Пользователь с такой парой Пароля и Логина в системе не зарегистрирован!'
            })
        if user and user.check_password(password) and user.is_active == False:
            raise forms.ValidationError('Ваша учётная запись не активирована! Дождитесь активации или позвоните для решения вопроса.',
                                        code='1no_active', )


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
        labels = {
            'first_name': 'Имя*',
            'last_name': 'Фамилия',
            'email': 'Email',
        }
        help_texts = {
            'first_name': '(можно и отчество)',
        }


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'phone', 'city', 'address', 'company']
        labels = {
            'image': 'Фото (изобржаение)',
            'phone': 'Телефон',
            'city': 'Населённый пункт',
            'address': 'Адрес',
            'company': 'Компания',
        }
        help_texts = {
            'image': '! максимальный размер 900х900px',
            'phone': 'актуальный и доступный для связи с вами',
            'city': 'город, село, хутор, аул и т.п.',
            'address': 'В случае доставки Транспортной Компанией, доставка будет именно на этот адрес',
            'company': 'Название компании, магазина, ООО, ОАО, ЗАО, ИП и т.п.'
        }


class ProfilePhoneForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['phone']
        labels = {
            'phone': 'Телефон',
        }
        help_texts = {
            'phone': 'мобильный, актуальный и доступный для связи с вами',
        }
    
    def clean(self):
        phone = self.cleaned_data['phone']
        
        if not phone.startswith('+7(9'):
            raise Http404
